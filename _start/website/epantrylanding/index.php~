<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<?php if($refresh) echo $refresh; ?><title>ePantry</title>
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Overlock:400,400italic,700,900">
		<link rel="apple-touch-icon" href="images/apple.png">
		<link rel="apple-touch-icon" sizes="72x72" href="images/apple72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="images/apple114.png">
		<script src="//cdn.optimizely.com/js/135693717.js"></script>
		<script type="text/javascript" src="scripts/jquery.js"></script>
		<script type="text/javascript" src="scripts/jquery.waypoints.js"></script>
		<script type="text/javascript" src="scripts/jquery.brcss.js"></script>
		<script type="text/javascript" src="scripts/jquery.validate.js"></script>
		<script type="text/javascript" src="scripts/jquery.fancybox.js"></script>
		<script type="text/javascript" src="scripts/script.js"></script>
		<!--[if !(IE 6)&(lt IE 9)]>
			<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lte IE 6]>
			<script>location.href='http://ie6countdown.com'</script>
			<style>body { background: #FFF; display: none; }</style>
		<![endif]-->
	</head>
	<body>
		<section id="top">
			<a name="antop"></a>
			<header>
				<div class="container">
					<a class="logo scroll access" href="#antop">ePantry</a>
					<nav>
						<ul>
							<li><a href="#anwhy" class="scroll"><span class="start"></span>Why ePantry?<span class="end"></span></a></li>
							<li><a href="#anstory" class="scroll"><span class="start"></span>Our Story<span class="end"></span></a></li>
							<li><a href="mailto&#58;c&#97;&#114;ee%72%73%40&#101;&#112;&#97;ntry&#46;c&#37;6F&#109;?subject=Work%20with%20Us">Work with Us</a></li>
              <li><a href="#alphas" class="fancybox">Alpha Members</a></li>
							<li><a href="http://epantry.wordpress.com/" class="fancybox">Blog</a></li>
						</ul>
					</nav>
					<div id="socialbar">
						<ul>
							<li><a href="https://twitter.com/epantry" target="_blank" class="access social twitter">Twitter</a></li>
							<li><a href="https://www.facebook.com/ePantry" target="_blank" class="access social facebook">Facebook</a></li>
							<li><a href="https://plus.google.com/116369139416762317307" target="_blank" class="access social google">Google Plus</a></li>
							<li><a href="http://www.linkedin.com/company/2738186" target="_blank" class="access social linkedin last">LinkedIn</a></li>
						</ul>
					</div>
				</div>
			</header>
			<div id="headlines" class="container centered">
				<p class="lighter">Making it easy for busy people to be responsible.</p>
				<h2>Put your household supplies<span class="leaf"></span> on autopilot.</h2>
			</div>
			<div class="ecostrip">
				<div class="container">
					<div class="steps">
						<div class="step">
							<h3>1. Start</h3>
							<div class="icon start"></div>
							<p>Share a few facts about your home so we can personalize your service.</p>
						</div>
						<div class="step">
							<h3>2. Customize</h3>
							<div class="icon customize"></div>
							<p>Choose the products you want to put on autopilot. All products are 100% good for you and the environment.</p>
						</div>
						<div class="step">
							<h3>3. Review</h3>
							<div class="icon review"></div>
							<p>We customized a schedule just for you. Review it. See your savings. Changes? <br />No problem.</p>
						</div>
						<div class="step last">
							<h3>4. Receive</h3>
							<div class="icon receive"></div>
							<p>Receive your products automatically based on your schedule. Changes? Still <br />no problem.</p>
						</div>
					</div>
				</div>
			</div>
			<div id="asterik" class="container">
				<p class="lighter centered">
					<span class="leaf"></span>
					Soaps, toilet paper, detergents &amp; all other products you wouldn&#8217;t mind someone else buying for you.
				</p>
			</div>
      <!--form id="beta" class="container centered" method="post" action="join.php"-->
			<form id="beta" class="container centered" method="post" action="<?=$_SERVER['PHP_SELF']; ?>">
				<div class="emailbar">
					<?php if(isset($message)): ?><input name="email" type="text" class="required email <?php echo $message['type']; ?>" value="<?php echo $message['value']; ?>" /><?php else: ?><input name="email" type="text" class="required email" /><?php endif; ?><input type="submit" value="Join!" />
				</div>
				<?php if(isset($message)): ?>
					<p class="messaging lighter<?php echo ' '.$message['type']; ?>"><?php echo $message['text']; ?></p>
				<?php else: ?>
					<p class="messaging lighter">Enter your email to join the ePantry public beta.</p>
				<?php endif; ?>
			</form>
		</section>
		<div id="alphas" class="popup">
			<h2>Alpha Members</h2>
			<p class="lighter">Get in touch! Ask a question, make any changes or just say hi!</p>
			<br /><br />
			<table>
				<tr>
					<td width="50">Email:</td>
					<td><a href='mail&#116;o&#58;%61lph&#37;61sup%&#55;0&#111;&#114;t%40%&#54;&#53;pa&#110;&#37;&#55;4r%7&#57;&#46;c%&#54;F&#109;'>alphasupp&#111;r&#116;&#64;&#101;p&#97;ntr&#121;&#46;com</a></td>
				</tr>
				<tr>
					<td>Phone:</td>
					<td><a href="tel:914-844-5102">914.844.5102</a></td>
				</tr>
			</table>
			<br /><br />
			<a href="javascript:void(0)" class="close">Close Window</a>
		</div>
		<section id="why" class="container">
			<h2 class="centered">Why ePantry?<a name="anwhy"></a></h2>
			<div id="reasons">
				<div class="reason">
					<h3>Easier</h3>
					<p>ePantry gets you exactly what you need, when you need it. Change your preferences whenever.</p>
				</div>
				<div class="reason">
					<h3>Better</h3>
					<p>ePantry is customized to <br />handle the unique details of your household goods.</p>
				</div>
				<div class="reason last">
					<h3>Greener</h3>
					<p>ePantry only gets you products free of chemicals harmful to your family and to the earth.</p>
				</div>
			</div>
		</section>
		<section id="story" class="container">
			<div class="papers">
				<div class="paper">
					<h2 class="centered">Our Story<a name="anstory"></a></h2>
					<p>ePantry was founded in 2012 with a vision to make the world a little
					more sustainable by making people&#8217;s lives a little easier. The
					earliest version of our idea came, as many ideas do, over a beer back
					at Amherst College. Despite considering myself &#8220;environmentally
					friendly,&#8221; I was frequently buying petroleum-based,
					non-compostable, never recycled plastic cups &#8230; even when more
					sustainable alternatives existed. Why? Convenience. Mass-market brands
					can get their products ideal shelf placement, where they are too easy
					to ignore.</p><br />
					<p>We started ePantry to change that dynamic by using ecommerce to change
					a category that is both inconvenient and has a massive environmental
					footprint: household consumables. Our goal is to create a solution so
					convenient (and economical), that people &#8211; regardless of their
					level of environmental passion &#8211; will want to use it. Can we be
					more convenient than the &#8220;paper towel isle&#8221; or stockpiling
					hundreds of rolls of toilet paper? I sure hope so &#8230;</p><br />
					<p>And, of course, all of the brands that we partner with share our
					mission of both protecting the planet and protecting families. We only
					offer products that are good for the world and for your family (which
					always includes being completely evil-chemical free).</p><br />
					<p>Thanks so much for being a part of this.  We hope we can make your
					life a little easier, and leave the world a little greener for it.</p><br />
					<p>Sincerely, <br />Team ePantry</p>
					<div class="recycle"></div>
				</div>
			</div>
		</section>
		<footer>
			<div class="container">
				<p class="lighter centered">&copy; Copyright <?php echo date('Y'); ?> ePantry</p>
			</div>
		</footer>
	</body>
</html>
