<?php

	if(empty($_POST))
	{
		require_once('index.php');
		exit();
	}
	else
	{
		$email = $_POST['email'];
		$async = intval($_POST['async']);
		$valid = filter_var($email, FILTER_VALIDATE_EMAIL);
		
		$message['value'] = $email;
		
		if($valid)
		{
			$message['type'] = 'valid';
			$message['text'] = 'Email received. Thanks!';
			
			$body = "A new email has been collected for beta testing.\n\n" . $email;
      $headers = "From: $email (Early Adopter)";
      $headers .= "\r\nMailed-By: epantry.com";
      $headers .= "\r\nReply-To: beta@epantry.com";
      $headers .= "\r\nX-Mailer: PHP/".phpversion();
			
			mail('savage@epantry.com, aharon@epantry.com, am@epantry.com, sl@epantry.com', 'ePantry Beta Tester', $body, $headers, '-fbeta@epantry.com');
			
			$body  = "Thank you for being an ePantry early adopter!  We're perfecting our service in private Alpha mode right now, ";
			$body .= "but we'll let you know as soon as we're ready to launch our Beta.  If you just can't wait and want to learn more ";
			$body .= "about becoming an Alpha Member (we love your enthusiasm!), please email us at alphasupport@epantry.com.\n\n";
			$body .= "-ePantry Team";
      $headers = 'From: earlyadopters@epantry.com';
      $headers .= "\r\nMailed-By: epantry.com";
      $headers .= "\r\nReply-To: earlyadopters@epantry.com";
      $headers .= "\r\nX-Mailer: PHP/".phpversion();
			
			mail($email, 'ePantry Early Adopter', $body, $headers, '-fearlyadopters@epantry.com');
			
			if($async)
			{
				echo $message['text'];
				exit();
			}
			else
			{
				$refresh = '<meta http-equiv="refresh" content="1;url='.'http://' . $_SERVER['HTTP_HOST']. '" />';
				$message['value'] = '';
				require_once('index.php');
				exit();
			}
		}
		else
		{
			if(!$async)
			{
				$message['type'] = 'error';
			
				if(empty($email)) $message['text'] = 'This field is required.';
				else $message['text'] = 'Valid email is required.';
			
				require_once('index.php');
				exit();
			}
		}
	}

?>
