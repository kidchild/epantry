$(document).ready(function()
{
	var headerOffset = 100;

	$.browserSelect();
	
	fitForFold();
	$(window).resize(fitForFold);
	
	$('.fancybox').fancybox();
	$('#alphas .close').click(function() { $.fancybox.close(); });
	
	var paper = $('<div class="paper">');
	
	$('#story .papers').prepend(paper);
	
	paper.css({position:'absolute', top:0});
	paper.css('-webkit-transform', 'rotate(-2deg)');
	paper.css('-moz-transform', 'rotate(-2deg)');
	paper.css('-ms-transform', 'rotate(-2deg)');
	paper.css('-o-transform', 'rotate(-2deg)');
	
	$('section').waypoint(function(event, direction)
	{
		var active = $(this);
		
		if(direction === "up") active = active.prev();
		if(!active.length) active = active.end();
		
		var anchorLink = $('a[href="#an' + active.attr('id') + '"]');
		
		$('.divet').removeClass('divet');
		anchorLink.addClass('divet');
	}, 
	
	{offset:headerOffset});
	
	$('a[href^="#an"]').click(function(event)
	{
		event.preventDefault();
		
		var url = this.href;
		var parts = url.split("#");
		var target = parts[1];

		var targetOffset = $('a[name="' + target + '"]').offset();
		var targetTop = targetOffset.top - headerOffset;

		$('html, body').stop().animate({scrollTop:targetTop}, 1000, 'swing');
	});
	
	$('form#beta').validate(
	{
		messages:{email:{email:'Valid email is required.'}},
		onkeydown:false, onkeyup:false, onclick:false, onkeypress:false,
		
		errorPlacement:function(error, element)
		{
			$('.messaging').text(error.text()).addClass('error');
		},
		
		submitHandler:function(form)
		{
			$('.messaging').html('Sending &#8230;').removeClass('error').addClass('valid');

      /* MailChimp API Integration
       * 
       * @date          29 2013 01 17:36:02
       * @nerdfiles
       */
			
      /*
      var email = escape($('form#beta input[name="email"]').val());

      $.ajax({
        url: 'inc/store-address.php',
        type: 'GET',
        data: 'ajax=true&double_optin=false&send_welcome=false&email_address=' + email + '&email=' + email,
        success: function(data, textStatus, xhr) {
					$('form#beta input[name="email"]').val('').attr('disabled', 'disabled');
					$('form#beta input[type="submit"]').attr('disabled', 'disabled');
					$('.messaging').html(data);
					$('form#beta').delay(1000).slideUp(1000, function()
					{
						$('form#beta input[name="email"]').removeAttr('disabled');
						$('form#beta input[type="submit"]').removeAttr('disabled');
						$.waypoints('refresh');
					});
        },
        error: function(xhr, textStatus, errorThrown) {
					$('.messaging').html(errorThrown + '! Please try again later.');
					$('form#beta input[name="email"]').val('');
        }
      });
      */

      /* @nerdfiles 29 2013 01 17:33:28 */
			$.ajax({
				url:'join.php',
				type:'POST',
				data:{email:$(form).find('input[name="email"]').val(), async:1},
				
				success:function(data, textStatus, xhr)
				{
					$('form#beta input[name="email"]').val('').attr('disabled', 'disabled');
					$('form#beta input[type="submit"]').attr('disabled', 'disabled');
					$('.messaging').html(data);
					$('form#beta').delay(1000).slideUp(1000, function()
					{
						$('form#beta input[name="email"]').removeAttr('disabled');
						$('form#beta input[type="submit"]').removeAttr('disabled');
						$.waypoints('refresh');
					});
				},
				
				error:function(xhr, textStatus, errorThrown)
				{
					$('.messaging').html(errorThrown + '! Please try again later.');
					$('form#beta input[name="email"]').val('');
				}
			});

		}
	});
});

function fitForFold()
{
	$('header').css({top:0, left:0, position:'fixed', zIndex:10});
	$('#headlines').css({marginTop:'104px'});
	$('#asterik').css({marginBottom:'100px'});
	
	if($(window).height() < 768)
	{
		$('#asterik').css({marginBottom:'25px'});
		$('#headlines').css({marginTop:'65px', marginBottom:'20px'});
	}
}
