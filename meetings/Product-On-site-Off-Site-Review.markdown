#Product On-site Off-Site Review

##Datetime

Stu, Jordan, Alex, Stephen  
29-2013-01 8:45

##Feedback Collection

1. Need "Cart", History rather: Breadcrumbish? There's a precedence in the Completeness Percentage. What about its UI real estate? How do products break down into Savings for an overall month?
2. [More Details & Product Image] CTAs can be reduced to intention tracking hover on the column itself. The user is primed to *click already.
3. Customer thought larger dollar signs indicated to save more money. Given brand, customer is primed to interpret all events as positive.
4. Assess consistency of modal window scheme. Perhaps merge the two latent workflow schemes? Some alert, some require control. Do modals have to break the back button? When?
5. Consider on "Select Soaps & Cleaners Essentials" a fat vertical CSS scroller, a fat menu. Perhaps consider double click?
6. Heavy text in modal windows can be a danger. (Consistency)
7. Delivery Schedule UI: Need clearer distinction of horizontal logical groups. What information is the user extract more immediately, and how to we establish consistency between delivery schedule online and on the thin-screen print outs (which could in theory become digital display products). Need to increase concision in copy.
	1. Customer felt intimated by tabular datawall. Need visual emphasis on first column or "the first month" ("Sticker shock"? #hypothesis How might visual emphasis minimize sticker shock?) This might be a cultural shift question; a kind of rite of passage. Can one implement an "epantry" over night? How does this long-term question affect the branding and persuasion copy?
	2. Shows "how much and when." (This is a lifestyle adaptation question; see [7.1].)
8. Customer suggests: phasing in strategy suggestions to fit into [1]. A phasing in involves a historical breakdown of sustainability habits. (Credibility of discount.)
9. Suggestion Retention: How can we allow customers to redact but understand the intention of ePantry's normative shopping workflows.
10. Shipment schemes, lifestyle modeled.
11. Getting back to the "shopping experience": The App is not a Brick and Mortor. Customers need incentives for fun to continue flows and be emotionally primed to digest marketing-loyalty building messages, like arrangement of free samples in isles of the store around certain "categories" of produce/products/brands. UI should spin Savings as "Free Samples" in the History-Breadcrumbish-Completeness system. How about an immediate visual feedback from D3.js style visualizations?
	1. \#ideas [Masonry](http://masonry.desandro.com/)?, [http://bartaz.github.com/impress.js](http://bartaz.github.com/impress.js), [http://tympanus.net/Tutorials/ExpandingOverlayEffect/index.html](http://tympanus.net/Tutorials/ExpandingOverlayEffect/index.html), Interactive Animated Vertical Lists in CSS3 for Product Lists ([http://lab.hakim.se/scroll-effects/](http://lab.hakim.se/scroll-effects/)), [Flyout Page Menu](http://lab.victorcoulon.fr/css/path-menu/)?
	2. \#ideas Can icons compress branding information, marketing and lifestyle suggestions? (Think logical positivism à la Neurath.)
	3. Consider the converse strategy to hide information used by Facebook: So, spin hiding important into a strategy for brand discovery: [5 Design Tricks Facebook Uses To Affect Your Privacy Decisions](http://techcrunch.com/2012/08/25/5-design-tricks-facebook-uses-to-affect-your-privacy-decisions/)
	4. \#ideas Maybe these ideas could be subsumed under "shopping notifications," like the minidigital displays that appear in physical grocery stores; Sensitivity to Couponer Lifestyles? Would this fall under the "professional shopper/guide" concept?
12. Customers expect information (metainformation) on the nature of their experience: precedent ui:[Completeness]. Progress bar isn't inviting, but could reframe or orient the way the customers interpet connected prompts. The Progress bar has an onus to repeat Clippy's mistakes or not.
13. Customers expect opinionated, focused variety. So, "rich frictionless shopping" #ideas.
	1. [http://www.wordnik.com/words/friction](http://www.wordnik.com/words/friction)less, [http://www.wordnik.com/words/scale](http://www.wordnik.com/words/friction)ed shopping experience.
	2. ...
14. Brand Discovery - Personal Philosophy: Talking past certain customers? (See the iconography problem again: [nounproject](http://thenounproject.com) for ideas.)
15. Clicking is a sensitive informative context.
16. "College Students" verses other lifestyles: How will this affect shopping flow and marketing opportunities?
17. Reward Phase? What is the model shopper? How far does one deviate from that at the end of the shopping experience? (Not to be aggressively critical but informatively critical -- think "opinionated" again. The app is a set of conventions, utilities, services, and opinions.)
18. Product differentiation: syzygy modeling (TN!!)
19. Soap question interpretation problems. 
20. We're priming customers to click: game mechanics are implied by workflows, "first-person" priming? What do we miss if we give the context of FPS games, but lack the UI hooks or tropes that are common to them? Do we miss brand discovery opportunities? Miss being as informative as we can be with the given real estate?
21. "Fast-Track" concept
22. Pet Owner Lifestyles, Adaptation, Sensitivity?
23. Need "guideful copy"; priming but not necessarily positive, certainly not negative.
	1. Professional shoppers realize that shoppers care more about their kids,  to organize their lifestyle around their kids. It's hard enough to switch to sustainable products just for oneself.
	2. Autopilot versus Concierge: #idea #TN Neural network training #see [http://harthur.github.com/brain/](http://harthur.github.com/brain/)  
	3. Questions should be rendered in a certain vernacular, etc.
24. Loyal Customers of Competitors. How do we adapt around existing shopping experiences? Traditional pre-online shopping experiences?
	1. **Conventions of Shopping**,
	2. **Utilities of Shopping**, 
	3. **Services of Shopping**, 
	4. **Opinions of Shopping**
25. Reset/Restart Wizard per User Created Profile: Jordan mentioned, "What is someone moves out of the house?" 

##Meeting Notes Collection

1. Alex's [Notes](https://mail.google.com/mail/u/1/?ui=2&view=btop&ver=lsvjwajrtlp4&search=inbox&th=13c87b5935ed3c8d&cvid=1)

##A/B Test Ideas

1. Cart Summary?
2. Dropping: "Overview"
3. Changing Form Structures around Household Biography Questions
4. Less language like "reward" where it implies we are marking up or determining price points. We're a search engine, a sustainability search engine. (Insider prices?)
	1. Loyalty to brands gets one a greater chance at a privileged relationship.
	2. Ease of experience as loyalty is developed; reward for being a reliable customer. Bulk Purchasing Discounts versus Brand Loyalty Discounts.
	3. Horizontal Bulk, Vertical Bulk Purchasing: How does this explain brand loyalty over time? How do we measure these intuitions of shoppers in terms of what they assume or expect the professional shopper will categorize them as?
	4. Nurturing Brand Loyalty, teaching Customers how to reap benefits from being loyal to brands; nurturing brand engagement.
	5. Brand Loyalty language conventions: http://www.wordnik.com/words/shaping, http://www.wordnik.com/words/pairing, http://www.wordnik.com/words/sustain, etc...
	6. "By engaging brands, you can shape how much you save." (We are here to nurture and promote brand engagement.)
	7. Supermarkets offer too much variety, for display purposes. Supermarkets are brand agnostic. ePantry allows for one to receive discounts as if brand engagement were equivalent to buying in bulk. Imagine a categorization of savings at checkout: multiple products of one brand should count toward savings on the group, like shipping books in bulk. "Brand buying is like Bulk buying: it should save you money!" 
		1. Is brand buying more sustainable than bulk buying?
		2. Do the suppliers/producers agree? -- They cheaply make certain goods, but have to compete in a differentiated market they cannot control the complexity of. In a way, loyalty helps indicate overall market success to companies.
		3. Become intimate with those items, brand loyalty more easily translates to sentimentalism. And then re-use of those branded products!
		4. $ | $$ | $: soem think the "$$" means saving more on savings sites, but may mean that its more costly by most shopping experience conventions. What if +$ | -$$ | -$ means that the "+" indicates that overall individual products are generally more experience than their category/class competitors.

##Sidenotes

1. FAQ needs a revamp. Dense copy.
2. ...

##Progress

1. Cred. of Discount
2. 