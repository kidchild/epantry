
window.front_end_utils ?= {}

front_end_utils.rando = ->
  randy = Math.ceil(Math.random() * 1000) + 2
  if randy < 400
    randy = 600+(2*Math.ceil((randy*1/100)))
  if randy > 900
    randy -= (300)
  return randy

products = []
count = 9

t_products = while count -= 1
  products.push({'name': 'p'+count, 'w': front_end_utils.rando(), 'h': front_end_utils.rando()})

products =
  'products': products

ui_formats =
  'formats': [
    { 'name': "accessible" },
    { 'name': "formal" },
    { 'name': "niche" }
  ]

categories =
  'categories': [
    { 'name': 'Hand Soap' },
    { 'name': 'Dish Soap' }
  ]

window.front_end ?= {}

front_end.init = ->
  front_end.some_ui_cta()
  #front_end_utils.rando()

front_end.some_ui_cta = ->
  a$ = $('.site-stage a')

  a$.click (e) ->
    self$ = $ @
    self$.closest('article')
      .addClass('selected')
        .siblings().removeClass('selected')

    $('.site-stage')
      .attr('id',
        self$.attr('href').replace('#', '')
      )

app = $.sammy('#main', ->
    @use Sammy.EJS
    main$ = $('#main')

    @get '#/', () ->
      @partial './_themes/eP/_templates/base-ui-formats.ejs', ui_formats, (html) ->
        main$.html(html)

    @get '#/categories', () ->
      @partial './_themes/eP/_templates/base-categories.ejs', categories, (html) ->
        main$.html(html)

    @get '#/products', () ->
      @partial './_themes/eP/_templates/base-products.ejs', products, (html) ->
        main$.html(html)
        front_end.some_ui_cta()
  )

$ ->
  app.run '#/'
  front_end.init()

