#Prototype Design Notes

1. Reduce number of CTAs to control focus and direction. Need a stronger differentiation than color. (Think of precedent of hyperlinks: Buttons submit forms, hyperlinks open pages; we should follow existing conventions.
2. Minimal iconography, no tiny fonting
3. Off-load text content to hover CTAs where available but make CTAs iconographic and easy to bump into when reading
4. Use Active Verb design concepts to emphasize what might be passive

##TODO

1. Add in minimal/subtle iconography for guidance/direction concept.
	1. [ModernUIIcons](http://modernuiicons.com/)
	2. [Iconic](http://somerandomdude.com/work/iconic/) !!
2. Using Sans Guilt MB?
3. 