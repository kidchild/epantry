Futurico - Free User Interface Elements Pack
Futurico was specially made for DesignModo by our friend Vladimir Kudinov. 

Futurico is licensed under a Creative Commons Attribution 3.0 Unported (CC BY 3.0)  (http://creativecommons.org/licenses/by/3.0/). 

You are allowed to use these elements anywhere you want, however we�ll highly appreciate if you will link to our website when you share them - http://designmodo.com

Release Date: 21.11.2011
Last Update: 07.05.2012
Version: 1.1

== Changelog ==

= 1.1 =
Fix a few bugs.
New elements added. - http://designmodo.com/wp-content/uploads/2011/11/Futurico-free-1-1.jpg


Thanks for supporting our website and enjoy!

Links:
http://designmodo.com/futurico
http://vladimirkudinov.com
http://rockablethemes.com

Pro Version:
http://designmodo.com/futuricopro

