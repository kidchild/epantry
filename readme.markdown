#[ePantry](http://www.epantry.com/) | [repo](https://gitlab.com/epantry), [ideas](https://gitlab.com/epantry/tree/master/ideas.markdown) |

##Mission

ePantry's mission is to shrink the environmental footprint of American homes by making sustainably produced household products the most convenient option for busy consumers.

##Profile

ePantry is an ecommerce/mcommerce start-up that makes the purchasing of sustainable household products easy, efficient and awesome.

Our technology provides each individual with customized ongoing delivery of branded sustainable consumables (e.g. soap, laundry detergent, paper towels) simplifying the process of never running out of the products she needs.

##Accessibility

###UI Hooks

$ e.pantry
