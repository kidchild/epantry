/*
CSS Browser Selector v0.4.0 (Nov 02, 2010)
Rafael Lima (http://rafael.adm.br)
http://rafael.adm.br/css_browser_selector
License: http://creativecommons.org/licenses/by/2.5/
Contributors: http://rafael.adm.br/css_browser_selector#contributors

Apdated into a jQuery extension by Robert Abramski
Contributors: http://www.robertabramski.com
*/

/*********************************************************************************
 
Available OS Codes [os]
----------------------------------------------------------------------------------

win				Microsoft Windows (all versions)
vista			Microsoft Windows Vista new
linux			Linux (x11 and linux)
mac 			Mac OS
freebsd			FreeBSD
ipod			iPod Touch
iphone			iPhone
ipad			iPad
webtv			WebTV
j2me			J2ME Devices like Opera mini
blackberry		BlackBerry
android			Google Android
mobile 			All mobile devices

Available Browser Codes [browser]
----------------------------------------------------------------------------------

ie				Internet Explorer (All versions)
ie8				Internet Explorer 8.x
ie7				Internet Explorer 7.x
ie6				Internet Explorer 6.x
ie5				Internet Explorer 5.x
gecko			Mozilla, Firefox (all versions), Camino
ff2				Firefox 2
ff3				Firefox 3
ff3_5			Firefox 3.5
ff3_6			Firefox 3.6
opera	 		Opera (All versions)
opera8			Opera 8.x
opera9			Opera 9.x
opera10			Opera 10.x
konqueror		Konqueror
webkit			Safari, NetNewsWire, OmniWeb, Shiira, Google Chrome
safari			Safari, NetNewsWire, OmniWeb, Shiira, Google Chrome
safari3			Safari 3.x
chrome			Google Chrome
iron			SRWare Iron

Extra Codes
----------------------------------------------------------------------------------

js				Available when js is enabled

*********************************************************************************/

(function($)
{
	$.extend
	({
		browserSelect:function(appendNode)
		{
			var ua = navigator.userAgent.toLowerCase(),
				is = function (t) { return ua.indexOf(t) > -1 },
				g = 'gecko',
				w = 'webkit',
				s = 'safari',
				o = 'opera',
				m = 'mobile',
				h = appendNode == null ? $(document.documentElement) : $(appendNode),
				b = 
					[
					 	(!(/opera|webtv/i.test(ua)) && /msie\s(\d)/.test(ua)) 
					 		? ('ie ie' + RegExp.$1) : is('firefox/2') ? g + ' ff2' 
					 		: is('firefox/3.5') ? g + ' ff3 ff3_5' : is('firefox/3.6') ? g + ' ff3 ff3_6' 
					 		: is('firefox/3') ? g + ' ff3' 
					 		: is('gecko/') ? g 
					 		: is('opera') ? o + (/version\/(\d+)/.test(ua) ? ' ' + o + RegExp.$1 
					 		: (/opera(\s|\/)(\d+)/.test(ua) ? ' ' + o + RegExp.$2 : '')) 
					 		: is('konqueror') ? 'konqueror' 
					 		: is('blackberry') ? m + ' blackberry' 
					 		: is('android') ? m + ' android' 
					 		: is('chrome') ? w + ' chrome' 
					 		: is('iron') ? w + ' iron' 
					 		: is('applewebkit/') ? w + ' ' + s + (/version\/(\d+)/.test(ua) ? ' ' + s + RegExp.$1 : '') 
					 		: is('mozilla/') ? g : '',
					 		
					 		  is('j2me') ? m + ' j2me' 
					 		: is('iphone') ? m + ' iphone' 
					 		: is('ipod') ? m + ' ipod' 
					 		: is('ipad') ? m + ' ipad' 
					 		: is('mac') ? 'mac' 
					 		: is('darwin') ? 'mac' 
					 		: is('webtv') ? 'webtv' 
					 		: is('win') ? 'win' + (is('windows nt 6.0') ? ' vista' : '') 
					 		: is('freebsd') ? 'freebsd' 
					 		: (is('x11') || is('linux')) ? 'linux' : '',
					 				
					 		'js'
					 ];
				
			return h.addClass(b.join(' '));
		}
	})
	
})(jQuery);