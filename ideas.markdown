#[ePantry](http://epantry.com) | [gen](https://epantrycheckout.herokuapp.com/generate), [ch samp1](https://epantrycheckout.herokuapp.com/payment?id=bW9vQG1vby5jYWFhYWFhYWFhYWFhYWFhYWE6MS4xMToyOjI6Mg==)

##Issue Tracking

1. Github Issues
	1. Minimalistic
2. Pivotal Tracker
	1. Heavy-lifter

##Wireframing

1. Pencil
2. gomockingbird
3. ...

##Prototyping

1. ...
2.	[Flairbuilder](http://www.flairbuilder.com/)  
    Interactivity combined with Wireframes.

##Browser Testing

1. Browserling
2. Selenium
3. Testling
4. [browserstack](http://www.browserstack.com/)

##A/B or "Split" Testing

1. Swab <- Py
2. SimpleAB <- Py
3. A/Bingo <- Rails
4. 7minabs <- Rails
5. Vanity <- Rails
6. PythonAB <- Py
7. Flask-Split <- Flask
8. django-lean <- Django

##Collaboration

1. Design: http://cageapp.com/
2. Development: http://chaoscollective.org/projects/builtinspace.html

##Infrastructure

1 week

1. enomaly
	1. Infrastructure-as-a-Service
2. AWS
3. Rackspace

##ecommerce

1-2 weeks

Build a non-linear checkout system into MV*. Let's say an "orbital checkout" system where "confirm checkout" is a question of "are you sure you want to 'purchase' (schedule) this item"? In the end, the user approves a schedule, so they should be allowed to be as messy as they like with their user experience. The UX itself should be minimal and allow for bridging of product-orbits-as-categories. So, we need to inspire: "Yes, I need to reconsider my garbage bag schedule/items" when the customer is indirectly thinking about bags due to searching for similarly contextualized "bags of the kitchen": so, consider if you will, all plastic bags that are "essential" to daily living, like zip-lock bags. The UX needs to promote patalogical connections like this. 

1. Dwolla
2. Noca
3. Stripe
4. Samurai

###Purchase Orbits

Plug these systems seamlessly into the flow of the "purchase orbits." To put it bluntly: purchasing a product should be as simple as clutching if one were in the store; as one surfs, one should simply be able to "schedule" the item without disruption of their flow. 

The "checkout" analogy resembles the brick-and-mortar store too much, like how Web design used to mirror Print. How can we step beyond re-using previous generation metaphors and actually introduce a new kind of experience? -- Does "mobile checkout" get it? Thieves never think of themselves as "checking out". The ideal experience might be similar to browsing a market square: one is expected be casual and comfortable; in similar fashion, trying out an item, or say picking up an orange to smell it, or even picking a grape from a shelf to continue shopping with a bit more nourishment, etc., this activity and actually "scheduling" or "purchasing" should require the same amount of cognitive effort from the searcher. We should reduce all user interface barriers to satisfying the customer's intent to _continue searching_ or _browsing_.

###Giving the customer insight about their products, over ads

Generally, use social media hooks based on customer activity (implied by the loading of social media CTAs via JavaScript) to give insight into how their activity within the product could transfer to interesting activity within adjacent social networks. Basically, a twitter feed becomes a new avenue for intelligent spamming. "Intelligence" comes from the manner in which the conversation via social media is postured. With the right posturing or language, a social media agent may view something that is technically spam as a viable potential need (and this may generate a click-thru). 

Does the app really need to present a "checkout schedule"? Or should that schedule, similar to memstash.co, update some existing "schedule" system for the customer's convenience? -- What if the app reminded them through social media venues, or allowed them to sign in through social media, giving them a "social grocer"? (Imagine a related twitter bot offers a "sample" once the API is triggered by the customer after hitting a product X page. The loading of the API itself is the notification that one has passed by a category of items. That is, cheese samples are most likely to be positioned or arranged next to crackers and breads, to give a grocer's suggestion to the customer. This analogy may carry over well into "social grocery."

###Panoramic Pantry

Inspire competition between customers but defer the competing grounds to social media fields. Customers are using Instagram to share their purchases; but who has the smallest footprint? Give customers a game theory style prompt to indirectly become more "altruistic" (that is, sustainable). The assumption here is that altruism and sustainability have a lot in common. What does one's footprint and efficient use of ePantry say about their overall dedication to sustainability?

##(MVP) Product

We're building a "lean" product that can be justified through gradual, methodical growth, so generally: 

1. Development Domain: [Build] -> [Measure] -> [Learn]
2. Design Domain: (Ideas) -> (Product) -> (Data)

That is,

1. How to [1].
	1. Coding.
	2. Analytics through Browser, Permissible Personally Identifiable Information
	3. A/B Testing successes and failures
2. How to [2].
	1. Raw brainstorming
	2. Product development
	3. Analysis of norms and trends, behavioral analysis

What does the W3C have to say here?

###CRM

"1. SugarCRM Inc. is the 800-pound gorilla in the open-source CRM category. Founded in 2004 by John Roberts, Clint Oram and Jacob Taylor, the Sugar open-source code has been downloaded more than 3 million times. The company has received $26 million in venture financing and employs more than 100 people. More than 12,000 companies use SugarCRM including Honeywell International, Starbucks Corp., First Federal Bank and BDO Seidman LLP. SugarCRM is written in PHP and is compatible with the MySQL database.

2. SplendidCRM Software Inc.'s development team formed in November 2005. The application is built on the Microsoft platform (Windows, ISS, SQL Server, C# and ASP). Designed for system integrators, SplendidCRM allows administrators to add user-customizable features such as .NET 2.0’s Themes, Web Parts and AJAX. SplendidCRM is positioned as a competitor to SugarCRM, as the two applications share many of the same features. For instance, both offer an Outlook plug-in and the ability to add custom fields.

3. CentricCRM has been around for seven years and has achieved a great deal of stability and robustness. In June 2007, CentricCRM (renamed Concursive as of December 2007) received investment funding from Intel Capital, the venture capital arm of Intel Corp. CentricCRM is aimed at the small-business market, although it has scaled up within Fortune 500 companies. Its more complex features can be turned off if they are not needed, and the administrative console allows for a great deal of customization. The free version comes with five user licenses. Centric CRM is written in Java and is compatible with MySQL databases." 
-- @see http://www.insidecrm.com/articles/crm-blog/the-top-10-opensource-crm-solutions-53507/

But [SugarCRM](http://www.sugarcrm.com/) is written for on PHP. There's a "trial."

####Other Open Source Alternatives

Python: [koalixcrm](https://github.com/scaphilo/koalixcrm/); to install:

#note A quick and dirty solution involves presenting this form to users via Web or e-mail. Have them subject their data using textarea element or Web form. Let Stripe handle the transaction.

These models can be updated as per the complexity of existing documents and their emerging schemas.

Ruby: [fat_free_crm](https://github.com/fatfreecrm/fat_free_crm)

Python: [django-crm](http://code.google.com/p/django-crm/)

####Front-ends

[KodeCRM](): Login [Demo](http://kodecrm.com/auth/login/demo/), Seller [Demo](http://kodecrm.com/dashboard/chat/), Shopper [Demo](http://184.106.134.49/oc_v1.4.9.3/)

#note Another service-based CRM, but functions on Backbone itself.

###Bookmarklet

1. If there's a relevant product on the page the user is currently on, use object detection to scrape those images of the current document (like stumbleupon) and feed this into the "social grocery" concept. Essentially, one's "ePantry" become embedded into their overall browsing experience. Candidly: "Did you remember to buy cookies? (After inspecting the cookies of a particular website.)
2. Moreover, the bookmarklet should leverage the "social grocery" of suppliers; so, it should behave intelligently whenever a supplier's social media network is activated. The most obvious example here is when the customer goes off-site to one of the supplier's Websites. To say the least, ePantry should be conscious of this, so that it might build the "social grocery" in general.

####Billboard Bookmarklet

If the user clicks on an anchor that contains an image and the anchor is set to @rel="external" then show the billboard. This might simulate that the customer has passed by a grocery store isle miniature billboard or "on-sale" item. Think of one of the core rules of shoplifting: never lift the item from the isle it was found.

##Accessibility

1. What are the UI hooks?
2. How should the copy be tailored?

##Mobile

1. Always load what-is-essential first (this is irrespective of device: so, minimum viable style in the CSS); then
2. Determine what to lazily load; then
3. Determine what the platform-consumption order should be.

##UI Concepts

1. Persistent "shelf"
2. Interactive organization of products
3. Map of locations which provide/deliver competitive sustainable-bulk options. Imagine having Last.fm radio (API) hooked into Google Maps, such that when the jukebox of the coffeeshoppe plays from a playlist, baristas could minimally like or dislike (or even present a mobile targeted app) songs. Imagine the details of goods exposed and categorized, and then presented through and integrated with the Google Maps API.
4. Delivery-organization-optimization service is the core (MVP?) of it. But it's a question of who is delivery what, and how. How do we expose these social interactions in an intuitive and elegant way in the flows of the sale process? -- We're looking at bringing to social awareness markets like Trader Joe's and smaller grocer venues. And hooking up a streamlined/social media delivery system. Some people will post tweets or instagrams of their packages. They're ordering sustainable bulk, so each sale and delivery is a very rich experience.
5. Integrating bike couriers; leverage [Leaflet](http://leafletjs.com/)/OpenStreetMap with low-cost GPI: global positioning (raspberry) pi where we update segmented KML geodata, uploaded from cyclist's connected system. Cache locally, upload when wifi is available. Show messenger/packager deliverer concurrency at the geocategorical level. This may all go into a tracking system for packages that may integrate with grocers (think a city-wide ordering app where one could in theory "order" used laundry detergent from a [good] neighbor -- as if one could "order" the salt from a nearby table in a diner, via twitter, etc.).
6. Discover hypermedia design opportunities (or just think hypermedia UI).
7. What is the drill-down for such a tweet?: "Hey, check out my #epantry! //domain.com/${sha1_hash_encoded_list_of_items}" -- Will it contain an image, a list of images? Some sites have combinatorial Cool URLs like "http://domain.com/${category}-${category}-${noun}-${noun}-${verb}" -- What can be learn from these uses of language and expression in the DNS space? How might such a Cool URL schema for ePantry determine tech stack opportunities and limitations?
8. "Opinionated, frictionless pantry surfing":
9. Hook up RDF/metadata/schema.org into Hypermedia payloads that could be indexed by search engines. Consider [syzygy](http://journal.webscience.org/491/) in the UI. Hypothesis: A richer dataset over products will give us a greater opportunity to measure custumer behavior and develop analytics-base.